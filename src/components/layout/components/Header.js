import React from "react";

export default function Header() {
  return (
    <header className="header">
      <div className="logo">
        <img src="" alt="Logo" />
      </div>
      <div className="col-1">
        <a href="/" className="title">
          <h2>Demo Streaming</h2>
        </a>
      </div>
      <div className="col-2">
        <a href="/" className="button">
          Log in
        </a>
        <a href="/" className="button trialBtn">
          Start your free trial
        </a>
      </div>
    </header>
  );
}
