import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Layout from "./components/layout";
import Home from "./screens/Home/";
import Movies from "./screens/Movies/";
import Series from "./screens/Series/";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Layout>
            <Route exact path="/" component={Home} />
            <Route path="/home" component={Home} />
            <Route path="/movies" component={Movies} />
            <Route path="/series" component={Series} />
          </Layout>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
