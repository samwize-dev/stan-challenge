import React, { useState, useEffect } from "react";
import Banner from "./components/Banner";
import sampleData from "../../feeds/sample.json";
import Utils from "../../utils/DemoUtils";

export default function Movies(props) {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    const moviesData = sampleData.entries
      .filter(
        (data) => data.programType === "movie" && data.releaseYear >= 2010
      )
      .slice(0, 21)
      .sort(Utils.compare);

    setMovies(moviesData);
  }, []);

  if (!movies) {
    return <p>Loading...</p>;
  }

  return (
    <div className="root">
      <Banner />

      <section className="section">
        {movies.map((item, i) => (
          <div key={i} className="section-item">
            <div
              className="card"
              style={{
                backgroundImage: `url(${item.images["Poster Art"].url})`,
              }}
            ></div>
            <div className="card-item">
              <a href={item.url}>
                <p>{item.title}</p>
              </a>
            </div>
          </div>
        ))}
      </section>
    </div>
  );
}
