import React, { useState, useEffect } from "react";
import Banner from "./components/Banner";
import sampleData from "../../feeds/sample.json";
import Utils from "../../utils/DemoUtils";

export default function Series(props) {
  const [series, setSeries] = useState([]);

  useEffect(() => {
    const seriesData = sampleData.entries
      .filter(
        (data) => data.programType === "series" && data.releaseYear >= 2010
      )
      .slice(0, 21)
      .sort(Utils.compare);

    setSeries(seriesData);
  }, []);

  if (!series) {
    return <p>Loading...</p>;
  }

  return (
    <div className="root">
      <Banner />

      <section className="section">
        {series.map((item, i) => (
          <div key={i} className="section-item">
            <div
              className="card"
              style={{
                backgroundImage: `url(${item.images["Poster Art"].url})`,
              }}
            ></div>
            <div className="card-item">
              <a href={item.url}>
                <p>{item.title}</p>
              </a>
            </div>
          </div>
        ))}
      </section>
    </div>
  );
}
