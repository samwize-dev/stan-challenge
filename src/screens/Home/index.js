import React from "react";
import Banner from "./components/Banner";
import { Section, SectionItem, Card, CardTile } from "./styles";

const features = [
  { id: 1, name: "Series", subtitle: "Popular Series", url: "/series" },
  { id: 2, name: "Movies", subtitle: "Popular Movies", url: "/movies" },
];

export default function Home(props) {
  return (
    <div className="root">
      <Banner />

      <Section>
        {features.map((item, i) => (
          <SectionItem key={i}>
            <Card>
              <h2>{item.name}</h2>
            </Card>
            <CardTile>
              <a href={item.url}>
                <p>{item.subtitle}</p>
              </a>
            </CardTile>
          </SectionItem>
        ))}
      </Section>
    </div>
  );
}
