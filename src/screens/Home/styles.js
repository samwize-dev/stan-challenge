import styled from "styled-components";

const Section = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  padding: 0.8rem 7rem;
  border: 1px solid #efefef;
  width: 100%;
  margin: 0 auto;
  box-sizing: border-box;
`;

const SectionItem = styled.div`
  color: #fff;
  margin: 0.5rem;
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;
  align-items: center;
  text-align: center;
  border: 1px solid #efefef;
`;

const Card = styled.div`
  background-color: #000;
  background-image: url(/assets/placeholder.png);
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center center;
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1 1 300px;
  width: 20rem;
  padding: 0.2rem 0.5rem;
  border: 1px solid green;
`;

const CardTile = styled.div`
  color: #000;
  padding: 1rem;
`;

export { Section, SectionItem, Card, CardTile };
