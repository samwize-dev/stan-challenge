## DEMO Stan Challenge

**Best projects filtering application ever**

### How did you decide on the technical and architectural choices used as part of your solution?

Keeping the rules in mind, i tried to restraint myself from using packages as its against the rules of the challenge given.
I think the challenge wasn't actually the application logic but the fact that i had to design it using custom css styling.

First of, i tried not to install too many dependencies but i needed to install just one which is a CSS-in-JS package i.e. styled-components. A package that would enable injection of css styles into components which is a rule of thumb for reusable components, but because this is just a small project using just the external styling did the job, as a larger global style would have been necessary.

I created a function for sorting by title, so rather than having that function within the component where it was used, i created a utils class for functions that can be reused across the application.

I also tried to use the functional lifestyles that comes with react e.g. useState & useEffect among others.

The Routing was very straight forward as it doesn't require any child routing, so the react-router-dom which is a powerful routing tool did that just fine.

I also thought that rather than style each component using separate styles, i thought we could have a generic styles in the index.css file that other components like the Home, Movies and Series could share, and then the component-specific styles in their own styles.css file.

The filtering had a priority order, what should come after the other, i had to start by filtering according to order of precedence, with projectType === "movies" && releaseYear >= "2010" before the splice(0, 21) and then finally sort by title

### Are there any improvements you could make to your submission?

Obviously, there are a lot of improvement i could have made if i had enough time under my belt, i could have used a few css framework like bootstrap, material-ui, tailwindcss or bulma but again it's out of the scope of this challenge.

I could have the filtering subjective to the user's choice using criteria like release date, project type or ordered by any of this fields.

Well, while still trying to achieve the listing of projects either by series or movies, i thought it could use a pagination.

There's quite an handful of improvement that could be done, and overtime could turn out to be a full fleshed application.

### What would you do differently if you were allocated more time?

I could have converted the sample.json into a mongodb collection and invoke the use of the fetch API or axios in getting its resources.

Made sure that the screens are responsive across all platforms and devices. Its either i used media-query to write different styles for a few important viewports
Do some other modifications with the css styling, create a bit of reusable components as i couldn't make out time to sepearte them into smaller components to enable maintenance and to ease readability.

There are quite a lot of thing i could think of doing to best develop this application using redux to maintain state in one place just in case the application begin to scale and becomes robust in the near future.

Usage of middleware like redux-thunk or saga, but i best prefer redux-thunk as it give a leverage in actions, and allows you return promises rather than mere object alone.

#### How to run the application

```
yarn start
```

#### Packages used

[styled-components](https://www.npmjs.com/package/styled-components)

#### Packages i could have used

[material-ui](https://www.npmjs.com/package/@material-ui/core)
[bulma](https://bulma.io/)

#### Github Link

[Github](https://github.com/samwizzy)

#### Project References

[EZone](https://dev.ezoneerp.com/)
